﻿using CreditCalculations.API.Models;
using CreditCalculations.FeeCalculations;
using Microsoft.AspNetCore.Mvc;

namespace CreditCalculations.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FeeCalculationsController : ControllerBase
    {
        private readonly IFeeCalculatorProvider _feeCalculatorProvider;

        public FeeCalculationsController(IFeeCalculatorProvider feeCalculatorProvider)
        {
            _feeCalculatorProvider = feeCalculatorProvider;
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public ActionResult<FeeCalculationResultModel> Calculate([FromBody]FeeCalculationParametersModel parameters)
        {
            var feeCalculator = _feeCalculatorProvider.Get(parameters.CustomerType);

            var result = feeCalculator.Calculate(new FeeCalculationParameters
            {
                CreditAmount = parameters.CreditAmount,
                NumberOfCreditsAlreadyTaken = parameters.NumberOfCreditsAlreadyTaken
            });

            return new FeeCalculationResultModel
            {
                Deductions = result.Deductions,
                AnnualFee = result.AnnualFee,
                BaseFee = result.BaseFee
            };
        }
    }
}
