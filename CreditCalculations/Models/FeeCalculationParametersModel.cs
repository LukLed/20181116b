using CreditCalculations.FeeCalculations;

namespace CreditCalculations.API.Models
{
    public class FeeCalculationParametersModel
    {
        public CustomerType CustomerType { get; set; }

        public decimal CreditAmount { get; set; }

        public int? NumberOfCreditsAlreadyTaken { get; set; }
    }
}