﻿using System;

namespace CreditCalculations.API.Models
{
    public class FeeCalculationResultModel
    {
        public decimal BaseFee { get; set; }

        public decimal Deductions { get; set; }

        public decimal AnnualFee { get; set; }
    }
}