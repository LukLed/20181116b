﻿using System;

namespace CreditCalculations.FeeCalculations
{
    public class FeeCalculationResult
    {
        public decimal BaseFee { get; set; }

        public decimal Deductions { get; set; }

        public decimal AnnualFee => Math.Max((decimal) (BaseFee - Deductions), 0);
    }
}