namespace CreditCalculations.FeeCalculations
{
    public class FeeCalculationParameters
    {
        public decimal CreditAmount { get; set; }

        public int? NumberOfCreditsAlreadyTaken { get; set; }
    }
}