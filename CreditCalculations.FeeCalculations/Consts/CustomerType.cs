﻿namespace CreditCalculations.FeeCalculations
{
    public enum CustomerType
    {
        Company = 1,
        PhysicalPerson = 2
    }
}
