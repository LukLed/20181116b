﻿using System;

namespace CreditCalculations.FeeCalculations
{
    public class PhysicalPersonFeeCalculator : IFeeCalculator
    {
        private const decimal Percentage = 18;

        private const decimal DeductionPerCredit = 400;

        private readonly IFeeCalculationParametersValidator _validator;

        public PhysicalPersonFeeCalculator(IFeeCalculationParametersValidator validator)
        {
            _validator = validator;
        }

        public FeeCalculationResult Calculate(FeeCalculationParameters parameters)
        {
            _validator.ValidateForPhysicalPerson(parameters);

            return new FeeCalculationResult
            {
                BaseFee = parameters.CreditAmount * Percentage / 100,
                Deductions = parameters.NumberOfCreditsAlreadyTaken.Value * DeductionPerCredit
            };
        }
    }
}