﻿using System;

namespace CreditCalculations.FeeCalculations
{
    public class CompanyFeeCalculator : IFeeCalculator
    {
        private const decimal Percentage = 10;

        private const decimal Deductions = 1000;

        private readonly IFeeCalculationParametersValidator _validator;

        public CompanyFeeCalculator(IFeeCalculationParametersValidator validator)
        {
            _validator = validator;
        }

        public FeeCalculationResult Calculate(FeeCalculationParameters parameters)
        {
            _validator.ValidateForCompany(parameters);

            return new FeeCalculationResult
            {
                BaseFee = parameters.CreditAmount * Percentage / 100,
                Deductions = Deductions
            };
        }
    }
}