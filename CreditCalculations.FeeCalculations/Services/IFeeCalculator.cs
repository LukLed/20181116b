﻿namespace CreditCalculations.FeeCalculations
{
    public interface IFeeCalculator
    {
        FeeCalculationResult Calculate(FeeCalculationParameters parameters);
    }
}
