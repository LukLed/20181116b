﻿namespace CreditCalculations.FeeCalculations
{
    public interface IFeeCalculatorProvider
    {
        IFeeCalculator Get(CustomerType customerType);
    }
}