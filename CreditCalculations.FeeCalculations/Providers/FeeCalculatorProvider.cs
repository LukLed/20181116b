using System;

namespace CreditCalculations.FeeCalculations
{
    public class FeeCalculatorProvider : IFeeCalculatorProvider
    {
        private readonly PhysicalPersonFeeCalculator _physicalPersonFeeCalculator;

        private readonly CompanyFeeCalculator _companyFeeCalculator;

        public FeeCalculatorProvider(PhysicalPersonFeeCalculator physicalPersonFeeCalculator, CompanyFeeCalculator companyFeeCalculator)
        {
            _physicalPersonFeeCalculator = physicalPersonFeeCalculator;
            _companyFeeCalculator = companyFeeCalculator;
        }

        public IFeeCalculator Get(CustomerType customerType)
        {
            switch (customerType)
            {
                case CustomerType.Company:
                    return _companyFeeCalculator;
                case CustomerType.PhysicalPerson:
                    return _physicalPersonFeeCalculator;
            }

            throw new ArgumentException("Invalid CustomerType.", nameof(customerType));
        }
    }
}