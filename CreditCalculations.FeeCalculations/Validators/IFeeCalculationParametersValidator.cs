﻿namespace CreditCalculations.FeeCalculations
{
    public interface IFeeCalculationParametersValidator
    {
        void ValidateForPhysicalPerson(FeeCalculationParameters parameters);

        void ValidateForCompany(FeeCalculationParameters parameters);
    }
}
