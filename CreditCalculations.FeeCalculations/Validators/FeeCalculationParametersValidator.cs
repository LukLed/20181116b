﻿using System;

namespace CreditCalculations.FeeCalculations
{
    public class FeeCalculationParametersValidator : IFeeCalculationParametersValidator
    {
        public void ValidateForPhysicalPerson(FeeCalculationParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentException("Parameters must be provided.", nameof(parameters));
            }

            if (parameters.CreditAmount <= 0)
            {
                throw new ArgumentException("CreditAmount should not be 0 or below.", nameof(parameters.CreditAmount));
            }

            if (parameters.NumberOfCreditsAlreadyTaken == null)
            {
                throw new ArgumentException("Please provide NumberOfCreditsAlreadyTaken.", nameof(parameters.NumberOfCreditsAlreadyTaken));
            }

            if (parameters.NumberOfCreditsAlreadyTaken < 0)
            {
                throw new ArgumentException("NumberOfCreditsAlreadyTaken should not below 0.", nameof(parameters.NumberOfCreditsAlreadyTaken));
            }
        }

        public void ValidateForCompany(FeeCalculationParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentException("Parameters must be provided.", nameof(parameters));
            }

            if (parameters.CreditAmount <= 0)
            {
                throw new ArgumentException("CreditAmount should not be 0 or below.", nameof(parameters.CreditAmount));
            }

            if (parameters.NumberOfCreditsAlreadyTaken < 0)
            {
                throw new ArgumentException("NumberOfCreditsAlreadyTaken should not be below 0.", nameof(parameters.NumberOfCreditsAlreadyTaken));
            }
        }
    }
}