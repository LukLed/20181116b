﻿using System;
using NUnit.Framework;

namespace CreditCalculations.FeeCalculations.Tests
{
    public class FeeCalculationParametersValidatorTests
    {
        [TestCase(0)]
        [TestCase(-100)]
        public void PhysicalPerson_IncorrectCreditAmount_ThrowsArgumentException(decimal creditAmount)
        {
            // Arrange
            var validator = new FeeCalculationParametersValidator();

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() =>
            {
                validator.ValidateForPhysicalPerson(new FeeCalculationParameters
                {
                    CreditAmount = creditAmount,
                    NumberOfCreditsAlreadyTaken = null
                });
            });
        }

        [TestCase(null)]
        [TestCase(-1)]
        public void PhysicalPerson_IncorrectNumberOfCreditsAlreadyTaken_ThrowsArgumentException(int? numberOfCreditsAlreadyTaken)
        {
            // Arrange
            var validator = new FeeCalculationParametersValidator();

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() =>
            {
                validator.ValidateForPhysicalPerson(new FeeCalculationParameters
                {
                    CreditAmount = 100,
                    NumberOfCreditsAlreadyTaken = numberOfCreditsAlreadyTaken
                });
            });
        }

        [Test]
        public void PhysicalPerson_EmptyParameters_ThrowArgumentException()
        {
            // Arrange
            var validator = new FeeCalculationParametersValidator();

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() =>
            {
                validator.ValidateForPhysicalPerson(null);
            });
        }

        [TestCase(0)]
        [TestCase(-100)]
        public void Company_IncorrectCreditAmount_ThrowsArgumentException(decimal creditAmount)
        {
            // Arrange
            var validator = new FeeCalculationParametersValidator();

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() =>
            {
                validator.ValidateForPhysicalPerson(new FeeCalculationParameters
                {
                    CreditAmount = 0,
                    NumberOfCreditsAlreadyTaken = null
                });
            });
        }

        [Test]
        public void Company_EmptyParameters_ThrowArgumentException()
        {
            // Arrange
            var validator = new FeeCalculationParametersValidator();

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() =>
            {
                validator.ValidateForCompany(null);
            });
        }
    }
}
