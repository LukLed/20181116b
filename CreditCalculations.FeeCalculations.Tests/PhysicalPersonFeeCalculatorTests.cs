using System;
using Moq;
using NUnit.Framework;

namespace CreditCalculations.FeeCalculations.Tests
{
    public class PhysicalPersonFeeCalculatorTests
    {
        [TestCase(1000, 0, 180, 0, 180)]
        [TestCase(10000, 2, 1800, 800, 1000)]
        [TestCase(1000, 2, 180, 800, 0)]
        public void GivenCorrectParameters_ReturnsCorrectValues(decimal creditAmount, int? numberOfCreditsAlreadyTaken, decimal baseFee, decimal deductions, decimal annualFee)
        {
            // Arrange
            var validatorMock = new Mock<IFeeCalculationParametersValidator>();
            var calculator = new PhysicalPersonFeeCalculator(validatorMock.Object);

            // Act
            var result = calculator.Calculate(new FeeCalculationParameters
            {
                CreditAmount = creditAmount,
                NumberOfCreditsAlreadyTaken = numberOfCreditsAlreadyTaken
            });

            // Assert
            Assert.AreEqual(baseFee, result.BaseFee);
            Assert.AreEqual(deductions, result.Deductions);
            Assert.AreEqual(annualFee, result.AnnualFee);
        }

        [Test]
        public void GivenParameters_RunsValidatorOnThem()
        {
            // Arrange
            var parameters = new FeeCalculationParameters
            {
                CreditAmount = 10000,
                NumberOfCreditsAlreadyTaken = 0
            };
            var validatorMock = new Mock<IFeeCalculationParametersValidator>(MockBehavior.Strict);
            validatorMock.Setup(v => v.ValidateForPhysicalPerson(parameters));
            var calculator = new PhysicalPersonFeeCalculator(validatorMock.Object);

            // Act
            calculator.Calculate(parameters);

            // Assert
            validatorMock.Verify(v => v.ValidateForPhysicalPerson(parameters), Times.Once());
        }
    }
}