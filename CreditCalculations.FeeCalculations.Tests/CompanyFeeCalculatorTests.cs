using Moq;
using NUnit.Framework;

namespace CreditCalculations.FeeCalculations.Tests
{
    public class CompanyFeeCalculatorTests
    {
        [TestCase(1000, null, 100, 0)]
        [TestCase(1000, 2, 100, 0)]
        [TestCase(10000, null, 1000, 0)]
        [TestCase(20000, null, 2000, 1000)]
        [TestCase(50000, null, 5000, 4000)]
        public void GivenCorrectParameters_ReturnsCorrectValues(decimal creditAmount, int? numberOfCreditsAlreadyTaken, decimal baseFee, decimal annualFee)
        {
            // Arrange
            var validatorMock = new Mock<IFeeCalculationParametersValidator>();
            var calculator = new CompanyFeeCalculator(validatorMock.Object);

            // Act
            var result = calculator.Calculate(new FeeCalculationParameters
            {
                CreditAmount = creditAmount,
                NumberOfCreditsAlreadyTaken = numberOfCreditsAlreadyTaken
            });

            // Assert
            Assert.AreEqual(baseFee, result.BaseFee);
            Assert.AreEqual(1000, result.Deductions);
            Assert.AreEqual(annualFee, result.AnnualFee);
        }

        [Test]
        public void GivenParameters_RunsValidatorOnThem()
        {
            // Arrange
            var parameters = new FeeCalculationParameters
            {
                CreditAmount = 10000,
                NumberOfCreditsAlreadyTaken = 0
            };
            var validatorMock = new Mock<IFeeCalculationParametersValidator>(MockBehavior.Strict);
            validatorMock.Setup(v => v.ValidateForCompany(parameters));
            var calculator = new CompanyFeeCalculator(validatorMock.Object);

            // Act
            calculator.Calculate(parameters);

            // Assert
            validatorMock.Verify(v => v.ValidateForCompany(parameters), Times.Once());
        }
    }
}